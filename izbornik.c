#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include "Header.h"


int izbornik(const char* const pacijenti) {
	
	FILE* fp;

	if ( (fp=fopen(pacijenti, "rb")) == NULL) {

		kreiranjeDatoteke(pacijenti);
	}


	printf("________________________________________\n");
	printf("Odaberite jednu od sljedecih opcija\n");
	printf("________________________________________\n");
	printf("\t\t\t Opcija 1: Dodavanje novih pacijenata u sustav\n");
	printf("\t\t\t Opcija 2: Prikaz svih pacijenata u sustavu\n");
	printf("\t\t\t Opcija 3: Pretrazivanje pacijenata\n");
	printf("\t\t\t Opcija 4: Uredjivanje podataka o pacijentu\n");
	printf("\t\t\t Opcija 5: Brisanje pacijenata iz sustava\n");
	printf("\t\t\t Opcija 6: Zavrsetak programa\n");
	printf("________________________________________\n");








	int uvijet = 0;

	static PACIJENT* poljePacijenata = NULL;
	static PACIJENT* pronadjeniPacijent = NULL;

	scanf("%d", &uvijet);

	switch (uvijet) {

	case 1:
		dodavanjePacijenta(pacijenti);
		break;


	case 2:

		if (poljePacijenata != NULL) {
			free(poljePacijenata);
			poljePacijenata = NULL;
		}
		poljePacijenata = (PACIJENT*)ucitavanjePacijenata(pacijenti);
		if (poljePacijenata == NULL) {
			exit(EXIT_FAILURE);
		}
		
		ispisivanje(poljePacijenata);
		
		break;

	case 3:

		if (poljePacijenata != NULL) {
			free(poljePacijenata);
			poljePacijenata = NULL;
		}
		poljePacijenata = (PACIJENT*)ucitavanjePacijenata(pacijenti);
		if (poljePacijenata == NULL) {
			exit(EXIT_FAILURE);
		}

		pretrazivanjePacijenata(poljePacijenata, pacijenti);

		break;


	case 6:

		izlaz(pacijenti, poljePacijenata);

		break;


	default:

		uvijet = 0;

	}

	return uvijet;
}
