#pragma once



#ifndef Header
#define Header
typedef struct pacijent {
	int Broj_zdravstvene_iskaznice;
	char Ime[20];
	char Prezime[20];
	char Adresa[30];
	int Broj_mobitela;
	char Napomena[1000];
	int Broj_primljenih_cijepiva;
	char Naziv_cijepiva[50];

}PACIJENT;

void kreiranjeDatoteke(const char* const pacijenti);
void dodavanjePacijenta(const char* const pacijenti);
void* ucitavanjePacijenata(const char* const pacijenti);
void ispisivanje(const PACIJENT* const poljePacijenata);
void* pretrazivanjePacijenata(PACIJENT* const poljePacijenata, const char* const pacijenti);
void dodavanjeNapomena(const char* const pacijenti);
void ispisivanjePodataka(const char* const pacijenti);
void cijepljenje(const char* const pacijenti);
int izlaz(const char* clanovi, PACIJENT* const poljePacijenata);
int provjera(const char* const pacijenti);
int izbornik2(const char* const pacijenti);


#endif


#ifndef Header
#define Header
typedef struct pacijent {
	int Broj_zdravstvene_iskaznice;
	char Ime[20];
	char Prezime[20];
	char Adresa[30];
	int Broj_mobitela;
	char Napomena[1000];
	int Broj_primljenih_cijepiva;
	char Naziv_cijepiva[50];

}PACIJENT;

void kreiranjeDatoteke(const char* const pacijenti);
void dodavanjePacijenta(const char* const pacijenti);
void* ucitavanjePacijenata(const char* const pacijenti);
void ispisivanje(const PACIJENT* const poljePacijenata);
void* pretrazivanjePacijenata(PACIJENT* const poljePacijenata);
void dodavanjeNapomena(const char* const pacijenti);
void ispisivanjePodataka(const char* const pacijenti);
void cijepljenje(const char* const pacijenti);
int izlaz(const char* clanovi, PACIJENT* const poljePacijenata);





#endif 
#pragma once
