#define _CRT_SECURE_NO_WARNINGS


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "Header.h"




static int brojPacijenata = 0;
static int pozicijaPacijenta = 0;


void kreiranjeDatoteke(const char* const pacijenti) {

	FILE* pf = NULL;
	pf = fopen(pacijenti, "wb");
	if (pf == NULL) {
		perror("Kreiranje datoteke pacijenti.bin");
		exit(EXIT_FAILURE);
	}
	fclose(pf);
}

void dodavanjeNapomena(const char* const pacijenti) {




	FILE* pf = fopen(pacijenti, "rb+");
	if (pf == NULL) {

		perror("Dodavanje napomena u datoteke pacijenti.bin");
		exit(EXIT_FAILURE);
	}
	else {

		fseek(pf, (sizeof(PACIJENT) * pozicijaPacijenta) + sizeof(int), SEEK_SET);
		PACIJENT temp = { 0 };
		fread(&temp, sizeof(PACIJENT), 1, pf);

		char tempS[1000];
		getchar();
		fgets(tempS, 1000, stdin);
		strcat(temp.Napomena, tempS);

		fseek(pf, (sizeof(PACIJENT) * pozicijaPacijenta) + sizeof(int), SEEK_SET);

		fwrite(&temp, sizeof(PACIJENT), 1, pf);
		rewind(pf);

		fclose(pf);
	}


}


void ispisivanjePodataka(const char* const pacijenti) {

	FILE* fp = fopen(pacijenti, "rb");
	if (fp == NULL) {
		perror("Dodavanje pacijenta u datoteke pacijenti.bin");
		exit(EXIT_FAILURE);
	}
	else {


		fseek(fp, (sizeof(PACIJENT) * pozicijaPacijenta) + sizeof(int), SEEK_SET);
		PACIJENT temp = { 0 };
		fread(&temp, sizeof(PACIJENT), 1, fp);

		printf("\t\t\tPacijent broj:%d\n___________________________________\n\n  Broj zdravstvene iskaznice:%d \n\n  Ime:%s\n\n  Prezime:%s\n\n  Broj mobitela:%d\n\n  Adresa:%s\n\n  Napomena:%s\n\n  Broj primljenih cijepiva:%d\n\n  Naziv cijepiva:%s\n___________________________________\n",

			pozicijaPacijenta + 1, temp.Broj_zdravstvene_iskaznice,
			temp.Ime,
			temp.Prezime,
			temp.Broj_mobitela,
			temp.Adresa,
			temp.Napomena,
			temp.Broj_primljenih_cijepiva,
			temp.Naziv_cijepiva



		);

		fclose(fp);
	}


}

void cijepljenje(const char* const pacijenti) {

	FILE* fp = fopen(pacijenti, "rb+");
	if (fp == NULL) {
		perror("Dodavanje pacijenta u datoteku");
		exit(EXIT_FAILURE);
	}
	else {
		fseek(fp, (sizeof(PACIJENT) * pozicijaPacijenta) + sizeof(int), SEEK_SET);
		PACIJENT temp = { 0 };
		fread(&temp, sizeof(PACIJENT), 1, fp);
		
		if (temp.Broj_primljenih_cijepiva > 2) {
			printf("Pacijent je primio sve doze");
		}
		else {
				temp.Broj_primljenih_cijepiva++;
				fseek(fp, (sizeof(PACIJENT) * pozicijaPacijenta) + sizeof(int), SEEK_SET);
				fwrite(&temp, sizeof(PACIJENT), 1, fp);
		}
		
		fclose(fp);

	}
}


void dodavanjePacijenta(const char* const pacijenti) {

	FILE* pf = fopen(pacijenti, "rb+");
	if (pf == NULL) {
		perror("Dodavanje pacijenta u datoteke pacijenti.bin");
		exit(EXIT_FAILURE);
	}

	else {
		fread(&brojPacijenata, sizeof(int), 1, pf);
		printf("Trenutni broj pacijenata: %d\n", brojPacijenata);
		PACIJENT temp = { 0 };
		//getchar();




		printf("Unesite broj zdravstvene iskaznice pacijenta\n");
		scanf("%d", &temp.Broj_zdravstvene_iskaznice);
		printf("Unesite ime pacijenta\n");
		scanf("%19s", temp.Ime);
		printf("Unesite prezime pacijenta\n");
		scanf("%19s", temp.Prezime);
		printf("Unesite adresu pacijenta\n");
		getchar();
		scanf("%29[^\n]", temp.Adresa);
		getchar();
		printf("Unesite broj mobitela\n");
		scanf("%d", &temp.Broj_mobitela);
		printf("Unesite napomenu vezane za pacijenta\n");
		getchar();
		fgets(temp.Napomena, 1000, stdin);
		int m;
		m = strlen(temp.Napomena);

		if (temp.Napomena[m - 1] == '\n') {
			temp.Napomena[m - 1] = '\0';
		}

		getchar();
		printf("Unesite naziv odabranog cijepiva\n");
		scanf("%49[\n]", temp.Naziv_cijepiva);
		getchar();


		fseek(pf, sizeof(PACIJENT) * brojPacijenata, SEEK_CUR);
		fwrite(&temp, sizeof(PACIJENT), 1, pf);
		rewind(pf);
		brojPacijenata++;
		fwrite(&brojPacijenata, sizeof(int), 1, pf);


		fclose(pf);
	}
}

void* ucitavanjePacijenata(const char* const pacijenti) {

	FILE* pF = fopen(pacijenti, "rb");
	if (pF == NULL) {
		perror("Ucitavanje pacijenata iz datoteke pacijenti.bin");
		return NULL;
		//exit(EXIT_FAILURE);
	}
	fread(&brojPacijenata, sizeof(int), 1, pF);
	printf("brojClanova: %d\n", brojPacijenata);
	PACIJENT* poljePacijenata = (PACIJENT*)calloc(brojPacijenata, sizeof(PACIJENT));
	if (poljePacijenata == NULL) {
		perror("Zauzimanje memorije za clanove");
		fclose(pF);
		return NULL;
		//exit(EXIT_FAILURE);
	}
	fread(poljePacijenata, sizeof(PACIJENT), brojPacijenata, pF);

	fclose(pF);
	return poljePacijenata;


}


void ispisivanje(const PACIJENT* const poljePacijenata) {


	if (poljePacijenata == NULL) {

		printf("Lista pacijenata je prazna\n");

		return;
	}

	int i;

	for (i = 0; i < brojPacijenata; i++) {

		printf("\t\t\tPacijent broj:%d\n___________________________________\n\n  Broj zdravstvene iskaznice:%d \n\n  Ime:%s\n\n  Prezime:%s\n\n  Broj mobitela:%d\n\n  Adresa:%s\n\n  Napomena:%s\n\n  Broj primljenih cijepiva:%d\n\n  Naziv cijepiva:%s\n___________________________________\n",

			i + 1, (poljePacijenata + i)->Broj_zdravstvene_iskaznice,
			(poljePacijenata + i)->Ime,
			(poljePacijenata + i)->Prezime,
			(poljePacijenata + i)->Broj_mobitela,
			(poljePacijenata + i)->Adresa,
			(poljePacijenata + i)->Napomena,
			(poljePacijenata + i)->Broj_primljenih_cijepiva,
			(poljePacijenata + i)->Naziv_cijepiva



		);


	}

}



void* pretrazivanjePacijenata(PACIJENT* const poljePacijenata, const char* const pacijenti) {

	if (poljePacijenata == NULL) {
		printf("Lista pacijenata je prazna\n");
		return NULL;
	}

	int i;
	int id;
	int uvjet = 1;
	printf("Unesite broj zdravstvene iskaznice \n");

	scanf("%d", &id);

	for (i = 0; i < brojPacijenata; i++) {

		if (id == ((poljePacijenata + i)->Broj_zdravstvene_iskaznice)) {

			printf("Pacijent je pronadjen");
			pozicijaPacijenta = i;
			while (uvjet) {
				uvjet = izbornik2(pacijenti);
			}

			return (poljePacijenata + i);
		}



	}

	

	return NULL;
}



int izlaz(const char* clanovi, PACIJENT* const poljePacijenata) {


	printf("Ukolko zelite zavrsiti program utipkajte 'da' u suprotnom utipkajte 'ne'\n");
	char potvrda[3];
	scanf("%2s", potvrda);

	if (!strcmp("da", potvrda)) {

		free(poljePacijenata);
		return 0;
	}

}

//int provjera(const char* const pacijenti) {
//
//	FILE* fp = fopen(pacijenti, "rb");
//	if (fp == NULL) {
//		return 1;
//	}
//	fclose(fp);
//}
